package com.hanko.cmn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hanko
 */
@SpringBootApplication
public class CmnApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmnApplication.class, args);
    }

}
